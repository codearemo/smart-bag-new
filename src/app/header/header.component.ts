import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoading;

  constructor(public loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  onLogOut() {
    this.loginService.loggedIn = false;
    localStorage.removeItem('userScreener');
    localStorage.removeItem('userBloodBank');
    this.router.navigate(['/login']);
  }

}
