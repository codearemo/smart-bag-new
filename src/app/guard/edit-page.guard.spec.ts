import { TestBed, async, inject } from '@angular/core/testing';

import { EditPageGuard } from './edit-page.guard';

describe('EditPageGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditPageGuard]
    });
  });

  it('should ...', inject([EditPageGuard], (guard: EditPageGuard) => {
    expect(guard).toBeTruthy();
  }));
});
