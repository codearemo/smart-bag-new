import { TestBed, async, inject } from '@angular/core/testing';

import { ScreenerAuthGuard } from './screener-auth.guard';

describe('ScreenerAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScreenerAuthGuard]
    });
  });

  it('should ...', inject([ScreenerAuthGuard], (guard: ScreenerAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
