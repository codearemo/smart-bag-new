import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../login.service';

@Injectable({
  providedIn: 'root'
})
export class ScreenerAuthGuard implements CanActivate {

  userScreener = {
    $class: '',
    firstName: '',
    lastName: '',
    screenerId: ''
  };

  constructor(private loginService: LoginService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('userScreener')) {
      this.loginService.loggedIn = true;
      return true;
    } else {
      this.router.navigate(['/login']);
      alert('Screening centre not logged in');
      return false;
    }
  }
}
