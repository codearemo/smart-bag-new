import { TestBed, inject } from '@angular/core/testing';

import { CodeSearchService } from './code-search.service';

describe('CodeSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CodeSearchService]
    });
  });

  it('should be created', inject([CodeSearchService], (service: CodeSearchService) => {
    expect(service).toBeTruthy();
  }));
});
