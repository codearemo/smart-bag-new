import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenedBloodViewComponent } from './screened-blood-view.component';

describe('ScreenedBloodViewComponent', () => {
  let component: ScreenedBloodViewComponent;
  let fixture: ComponentFixture<ScreenedBloodViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenedBloodViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenedBloodViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
