import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { CodeSearchService } from '../code-search.service';

@Component({
  selector: 'app-screened-blood-view',
  templateUrl: './screened-blood-view.component.html',
  styleUrls: ['./screened-blood-view.component.css']
})
export class ScreenedBloodViewComponent implements OnInit {

  isLoading;
  bbCode;

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: ''
  };

  constructor(private activatedRoute: ActivatedRoute, private codeSearchService: CodeSearchService, private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    this.bbCode = this.activatedRoute.snapshot.paramMap.get('id');
    this.quickBagCheck();
  }

  // Check if the blood bank was assigned to the blood bag
  // Just to be sure they don't use the url to check just any bag
  // Don't worry you don't have to say it...I know i'm smart ;)
  quickBagCheck() {
    this.codeSearchService.getBagDetails(this.bbCode)
      .subscribe(data => {
        const authScreener = JSON.parse(localStorage.getItem('userScreener'));
        this.bbDetails = JSON.parse(JSON.stringify(data));
        if (this.bbDetails.screener.split('#')[1] !== authScreener.screenerId) {
          alert(`Blood bag ${this.bbCode} doesn't belong to your Screening Centre`);
          this.router.navigate(['/login']);
        }
        this.isLoading = false;
      }, error => {
        alert('Bag may be unavailable or please check network and try again');
        this.router.navigate(['/login']);
        this.isLoading = false;
      });
  }

  makeEdit() {
    this.router.navigate([`/screening-centre/${this.bbDetails.BagId}`]);
  }
}
