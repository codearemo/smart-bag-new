import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CodeSearchService {

  isLoading = true;
  constructor(private http: HttpClient) { }

  getBagDetails(bbTagNumber) {
    this.isLoading = true;
    return this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Bag/${bbTagNumber}`);
  }
}
