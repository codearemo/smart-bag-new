import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { CodeSearchService } from '../code-search.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-bloodbank',
  templateUrl: './bloodbank.component.html',
  styleUrls: ['./bloodbank.component.css']
})
export class BloodbankComponent implements OnInit {

  confirmResults = false;
  isLoading;
  bbCode;
  screeningCentres;
  // I used this variable cause i wasn't sure why the screener in the bbDetails didn't work
  myscreener;

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: '',
    DonationDate: ''
  };


    // tslint:disable-next-line:max-line-length
  constructor(private activatedRoute: ActivatedRoute, private codeSearchService: CodeSearchService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.isLoading = true;
    this.bbCode = this.activatedRoute.snapshot.paramMap.get('id');
    this.callScreeningCentres();
    this.quickBagCheck();
    this.isLoading = false;
  }


  // Call all screening centres
  callScreeningCentres() {
    this.http.get('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Screeningcenter')
      .subscribe(data => {
        this.screeningCentres = data;
      }, error => {
        console.log(error);
      });
  }

  // Check if the blood bank was assigned to the blood bag
  // Just to be sure they don't use the url to check just any bag
  // Don't worry you don't have to say it...I know i'm smart ;)
  quickBagCheck() {
    this.codeSearchService.getBagDetails(this.bbCode)
    .subscribe(data => {
      this.bbDetails = JSON.parse(JSON.stringify(data));
      this.myscreener = this.bbDetails.screener.split('#')[1];

      const authBloodbank = JSON.parse(localStorage.getItem('userBloodBank'));
      this.bbDetails = JSON.parse(JSON.stringify(data));
      if (this.bbDetails.owner.split('#')[1] !== authBloodbank.bankId) {
        alert(`Blood bag ${this.bbCode} doesn't belong to your blood bank`);
        this.router.navigate(['/login']);
      }
    }, error => {
      alert('Bag may be unavailable or please check network and try again');
      this.router.navigate(['/login']);
    });
  }

  sendResult() {
    this.bbDetails.screener = 'resource:org.lifebank.smartbag.Screeningcenter#' + this.myscreener;
    this.isLoading = true;
    // Post the details from the blood bank about the screening centre and the date of donation
    // tslint:disable-next-line:max-line-length
    this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/AllocateScreener', {'$class': 'org.lifebank.smartbag.AllocateScreener', 'bagname': 'org.lifebank.smartbag.Bag#' + this.bbDetails.BagId, 'newScreener': this.bbDetails.screener})
      .subscribe(data => {
        // tslint:disable-next-line:max-line-length
        this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateDOD', {'$class': 'org.lifebank.smartbag.UpdateDOD', 'bagname': 'org.lifebank.smartbag.Bag#' + this.bbDetails.BagId, 'newDOD': this.bbDetails.DonationDate})
        .subscribe(DODdata => {
          this.router.navigate([`/blood-bank/view/${this.bbDetails.BagId}`]);
          this.isLoading = false;
        }, error => {
          alert('Error in selecting Date of Birth');
          this.isLoading = false;
        });
      }, error => {
        console.log(error);
        alert('Screening centre not asigned please try again');
        this.isLoading = false;
      });
  }

  bloodBankResultSubmit() {
    this.confirmResults = true;
  }

}
