import { Component, OnInit } from '@angular/core';
import { CodeSearchService } from '../code-search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '../../../node_modules/@angular/common/http';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  isLoading;
  screeningCentres;
  bloodBanks;
  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: '',
    DonationDate: ''
  };

  constructor(
    private codeSearch: CodeSearchService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.isLoading = this.codeSearch.isLoading;
    this.loadData();
  }

  loadData() {
    const bbTagNumber = this.activatedRoute.snapshot.paramMap.get('id');
    this.codeSearch.getBagDetails(bbTagNumber)
    .subscribe(
      data => {
        this.bbDetails = JSON.parse(JSON.stringify(data));
        if (this.bbDetails.safety === 'None') {
          alert('Bag hasn\'t been screened');
          this.router.navigate(['/']);
        }
        // tslint:disable-next-line:max-line-length
        this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Screeningcenter/${this.bbDetails.screener.split('#')[1]}`)
          .subscribe(sceeningCentres => {
            this.screeningCentres = sceeningCentres;
            },
            error => {
              console.log(error);
            }
          );
        this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/BloodBank/${this.bbDetails.owner.split('#')[1]}`)
          .subscribe(bloodBanks => {
            this.bloodBanks = bloodBanks;
            },
            error => {
              console.log(error);
            }
          );
        this.isLoading = false;
      },
      error => {
        this.router.navigate(['/invalid-bag']);
        this.isLoading = false;
      }
    );
  }
}
