import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../login.service';
import { CodeSearchService } from '../code-search.service';
import { Router } from '../../../node_modules/@angular/router';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { BrowserQRCodeReader, VideoInputDevice } from '@zxing/library';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  result;
  showVid;

  codeReader = new BrowserQRCodeReader();

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: ''
  };

  screeningCentre = {
    screenerId: '',
    firstName: '',
    lastName: ''
  };

  bloodBank = {
    bankId: '',
    firstName: '',
    lastName: ''
  };

  signInDetails = {
    smartTagNum: '',
    uniqueID: '',
    accessLevel: ''
  };

  isLoading = false;

  @ViewChild('fileInput') fileInput: ElementRef;

  // tslint:disable-next-line:max-line-length
  constructor(private loginService: LoginService, private codeSearchService: CodeSearchService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
  }

  onSignIn() {
    this.isLoading = this.codeSearchService.isLoading;
    // Check if smart tag number exist
    this.codeSearchService.getBagDetails(this.signInDetails.smartTagNum)
      .subscribe(data => {
        this.bbDetails = JSON.parse(JSON.stringify(data));
        // tslint:disable-next-line:max-line-length
        if (this.bbDetails.safety === 'None' && this.signInDetails.accessLevel === 'screeningcentre' && this.signInDetails.uniqueID === this.bbDetails.screener.split('#')[1]) {
          // tslint:disable-next-line:max-line-length
          this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Screeningcenter/${this.bbDetails.screener.split('#')[1]}`)
            .subscribe(screenerData => {
              this.screeningCentre = JSON.parse(JSON.stringify(screenerData));
              localStorage.setItem('userScreener', JSON.stringify(this.screeningCentre));
              this.router.navigate([`/screening-centre/${this.bbDetails.BagId}`]);
              this.isLoading = false;
            }, error => {
              alert('Please check network');
              this.isLoading = false;
            });
          // tslint:disable-next-line:max-line-length
        } else if (this.bbDetails.safety === 'None' && this.signInDetails.accessLevel === 'bloodbank' && this.signInDetails.uniqueID === this.bbDetails.owner.split('#')[1]) {
          // tslint:disable-next-line:max-line-length
          this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/BloodBank/${this.bbDetails.owner.split('#')[1]}`)
            .subscribe(bloodbankData => {
              this.bloodBank = JSON.parse(JSON.stringify(bloodbankData));
              localStorage.setItem('userBloodBank', JSON.stringify(this.bloodBank));
              this.router.navigate([`/blood-bank/${this.bbDetails.BagId}`]);
              this.isLoading = false;
            }, error => {
              alert('Please check network');
              this.isLoading = false;
            });
          // tslint:disable-next-line:max-line-length
        } else if (this.signInDetails.uniqueID === this.bbDetails.owner.split('#')[1] || this.signInDetails.uniqueID === this.bbDetails.screener.split('#')[1]) {
          this.loginService.login(this.signInDetails.accessLevel, this.signInDetails.uniqueID, this.signInDetails.smartTagNum);
        } else {
          alert('Blood bag code isn\'t registered to unique ID');
        }
        this.isLoading = false;
      },
        error => {
          alert('Invalid Tag Number or check network');
          this.isLoading = false;
        }
      );
  }

  makeScan() {
    this.isLoading = false;
    this.showVid = true;

    this.codeReader.getVideoInputDevices()
      .then(videoInputDevices => {
        if (videoInputDevices.length === 1) {
          const firstDeviceId = videoInputDevices[0].deviceId;
          this.codeReader.decodeFromInputVideoDevice(firstDeviceId, 'video')
          .then(result => {
              this.result = result;
              this.router.navigate([`/summary/${this.result.text}`]);
            })
            .catch(err => console.log(err));
        } else {
          const firstDeviceId = videoInputDevices[1].deviceId;
          this.codeReader.decodeFromInputVideoDevice(firstDeviceId, 'video')
          .then(result => {
              this.result = result;
              this.router.navigate([`/summary/${this.result.text}`]);
            })
            .catch(err => console.log(err));
        }
      })
      .catch(err => console.log(err));
  }

}
