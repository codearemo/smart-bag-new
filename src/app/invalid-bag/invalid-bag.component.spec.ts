import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidBagComponent } from './invalid-bag.component';

describe('InvalidBagComponent', () => {
  let component: InvalidBagComponent;
  let fixture: ComponentFixture<InvalidBagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidBagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidBagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
