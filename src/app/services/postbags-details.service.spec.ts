import { TestBed, inject } from '@angular/core/testing';

import { PostbagsDetailsService } from './postbags-details.service';

describe('PostbagsDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostbagsDetailsService]
    });
  });

  it('should be created', inject([PostbagsDetailsService], (service: PostbagsDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
