import { TestBed, inject } from '@angular/core/testing';

import { BagCheckService } from './bag-check.service';

describe('BagCheckService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BagCheckService]
    });
  });

  it('should be created', inject([BagCheckService], (service: BagCheckService) => {
    expect(service).toBeTruthy();
  }));
});
