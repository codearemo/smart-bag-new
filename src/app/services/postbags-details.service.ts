import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PostbagsDetailsService {

  constructor(private http: HttpClient) { }

  updateBagTestResult(val, testName, newValue) {
    if (testName === 'HIV') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateHIV', {'bagname': val, 'newHIV': newValue, '$class': 'org.lifebank.smartbag.UpdateHIV'});
    }
    if (testName === 'hepatitisb') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateHepatitisB', {'bagname': val, 'newHepatitisB': newValue, '$class': 'org.lifebank.smartbag.UpdateHepatitisB'});
    }
    if (testName === 'hepatitisc') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateHepatitisc', {'bagname': val, 'newHepatitisc': newValue, '$class': 'org.lifebank.smartbag.UpdateHepatitisc'});
    }
    if (testName === 'syphillis') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateSpyhills', {'bagname': val, 'newSpyhills': newValue, '$class': 'org.lifebank.smartbag.UpdateSpyhills'});
    }
    if (testName === 'HCV') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateHCV', {'bagname': val, 'newHCV': newValue, '$class': 'org.lifebank.smartbag.UpdateHCV'});
    }
    if (testName === 'Safety') {
      // tslint:disable-next-line:max-line-length
      return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateSafety', {'bagname': val, 'newsafety': newValue, '$class': 'org.lifebank.smartbag.UpdateSafety'});
    }
  }

  updateBloodType(val, bagName) {
    // tslint:disable-next-line:max-line-length
    return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdateBloodType', {'$class': 'org.lifebank.smartbag.UpdateBloodType', 'bagname': bagName, 'newBtype'  : val});
  }

  updatePCV(newVal, bagName) {
    // tslint:disable-next-line:max-line-length
    return this.http.post('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/UpdatePVC', {'$class': 'org.lifebank.smartbag.UpdatePVC', 'bagname': bagName, 'newPVC': newVal});
  }
}
