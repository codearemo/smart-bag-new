import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { CodeSearchService } from '../code-search.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-blood-bank-view',
  templateUrl: './blood-bank-view.component.html',
  styleUrls: ['./blood-bank-view.component.css']
})
export class BloodBankViewComponent implements OnInit {

  isLoading;
  bbCode;
  screeners;
  screenerName;

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: '',
    DonationDate: ''
  };


  // tslint:disable-next-line:max-line-length
  constructor(private activatedRoute: ActivatedRoute, private codeSearchService: CodeSearchService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.isLoading = true;
    this.bbCode = this.activatedRoute.snapshot.paramMap.get('id');
    this.quickBagCheck();
  }

  // Check if the blood bank was assigned to the blood bag
  // Just to be sure they don't use the url to check just any bag
  // Don't worry you don't have to say it...I know i'm smart ;)
  quickBagCheck() {
    this.codeSearchService.getBagDetails(this.bbCode)
      .subscribe(data => {
        const authBloodbank = JSON.parse(localStorage.getItem('userBloodBank'));
        this.bbDetails = JSON.parse(JSON.stringify(data));

        this.http.get('http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Screeningcenter')
          .subscribe(screeners => {
            // tslint:disable-next-line:forin
            for (const screener in screeners) {
              if (screeners[screener].screenerId === this.bbDetails.screener.split('#')[1]) {
                this.screenerName = screeners[screener].ScreenerName;
              }
            }
          }, error => {
            console.log(error);
          });

        if (!(this.bbDetails.owner.split('#')[1] === authBloodbank.bankId)) {
          alert(`Blood bag ${this.bbCode} doesn't belong to your blood bank`);
          this.router.navigate(['/login']);
        }
        this.isLoading = false;
      }, error => {
        alert('Bag may be unavailable or please check network and try again');
        this.router.navigate(['/login']);
        this.isLoading = false;
      });
  }

  makeEdit() {
    this.router.navigate([`/blood-bank/${this.bbDetails.BagId}`]);
  }

}
