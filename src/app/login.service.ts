import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ScreeningCentreModel } from './model/screeningcentre.model';
import { BloodBankModel } from './model/bloodbank.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loggedIn;

  screeningCentre = {
    screenerId: '',
    firstName: '',
    lastName: ''
  };

  bloodBank = {
    bankId: '',
    firstName: '',
    lastName: ''
  };

  constructor(private router: Router, private http: HttpClient) { }


  login(accesLevel, uniqueID, smartTagNum) {
    if (accesLevel === 'screeningcentre') {
      // Check if screeening centre exist in database
      this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/Screeningcenter/${uniqueID}`)
        .subscribe(data => {
          this.screeningCentre = JSON.parse(JSON.stringify(data));
          if (uniqueID === this.screeningCentre.screenerId) {
            this.saveScreener(this.screeningCentre);
            this.router.navigate([`/screened-blood/${smartTagNum}`]);
            this.loggedIn = true;
          } else {
            alert('Invalid Unique ID');
          }
        }, error => {
          alert('Invalid code combinations or check network');
        });
    } else if (accesLevel === 'bloodbank') {
      this.http.get(`http://ec2-18-188-48-177.us-east-2.compute.amazonaws.com:3000/api/BloodBank/${uniqueID}`)
        .subscribe(data => {
          this.bloodBank = JSON.parse(JSON.stringify(data));
          if (uniqueID === this.bloodBank.bankId) {
            this.saveuserBloodbank(this.bloodBank);
            this.router.navigate([`/blood-bank/view/${smartTagNum}`]);
            this.loggedIn = true;
          } else {
            alert('Invalid Unique ID');
          }
        }, error => {
          alert('Invalid code combinations or check network');
        });
    } else {
      alert('Some Details were incorrect');
    }
  }

  saveScreener(userScreener) {
    localStorage.setItem('userScreener', JSON.stringify(userScreener));
  }

  saveuserBloodbank(userBloodBank) {
    localStorage.setItem('userBloodBank', JSON.stringify(userBloodBank));
  }
}
