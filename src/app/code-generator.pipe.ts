import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'codeGenerator'
})
export class CodeGeneratorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.toString().split('#')[1];
  }

}
