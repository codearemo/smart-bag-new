import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { BcSearchComponent } from './bc-search/bc-search.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SummaryComponent } from './summary/summary.component';
import { AppRoutingModule } from './/app-routing.module';
import { CodeGeneratorPipe } from './code-generator.pipe';
import { Error404Component } from './error404/error404.component';
import { InvalidBagComponent } from './invalid-bag/invalid-bag.component';
import { ScreeningcentreComponent } from './screeningcentre/screeningcentre.component';
import { BloodbankComponent } from './bloodbank/bloodbank.component';
import { LoginService } from './login.service';
import { CodeSearchService } from './code-search.service';
import { AuthGuard } from './guard/auth.guard';
import { ScreenedBloodViewComponent } from './screened-blood-view/screened-blood-view.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ScreenerBloodbagEditComponent } from './screener-bloodbag-edit/screener-bloodbag-edit.component';
import { BloodBankViewComponent } from './blood-bank-view/blood-bank-view.component';

@NgModule({
  declarations: [
    AppComponent,
    BcSearchComponent,
    LoginComponent,
    HeaderComponent,
    SummaryComponent,
    CodeGeneratorPipe,
    Error404Component,
    InvalidBagComponent,
    ScreeningcentreComponent,
    BloodbankComponent,
    ScreenedBloodViewComponent,
    ManageUsersComponent,
    ScreenerBloodbagEditComponent,
    BloodBankViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [LoginService, CodeSearchService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
