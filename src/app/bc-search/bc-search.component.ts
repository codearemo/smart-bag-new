import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-bc-search',
  templateUrl: './bc-search.component.html',
  styleUrls: ['./bc-search.component.css']
})
export class BcSearchComponent implements OnInit {

  invalidCode = '';

  constructor(private router: Router, public loginService: LoginService) { }

  ngOnInit() {
  }

  onSubmit(bbCode: NgForm) {
    if (bbCode.controls.bloodBagCode.invalid) {
      this.invalidCode = 'Invalid code format please check';
      return;
    }
    this.router.navigate([`/summary/${bbCode.controls.bloodBagCode.value}`]);
  }

  onLogOut() {
    this.loginService.loggedIn = false;
    localStorage.removeItem('userScreener');
    localStorage.removeItem('userBloodBank');
  }

}


