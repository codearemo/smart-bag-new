import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BcSearchComponent } from './bc-search.component';

describe('BcSearchComponent', () => {
  let component: BcSearchComponent;
  let fixture: ComponentFixture<BcSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BcSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BcSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
