import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BcSearchComponent } from './bc-search/bc-search.component';
import { SummaryComponent } from './summary/summary.component';
import { LoginComponent } from './login/login.component';
import { InvalidBagComponent } from './invalid-bag/invalid-bag.component';
import { Error404Component } from './error404/error404.component';
import { ScreeningcentreComponent } from './screeningcentre/screeningcentre.component';
import { BloodbankComponent } from './bloodbank/bloodbank.component';
import { ScreenedBloodViewComponent } from './screened-blood-view/screened-blood-view.component';
import { AuthGuard } from './guard/auth.guard';
import { ScreenerAuthGuard } from './guard/screener-auth.guard';
import { BloodBankViewComponent } from './blood-bank-view/blood-bank-view.component';
// import { ScreenerBloodbagEditComponent } from './screener-bloodbag-edit/screener-bloodbag-edit.component';

const routes: Routes = [
  { path: 'invalid-bag', component: InvalidBagComponent},
  { path: 'screened-blood/:id', canActivate: [ScreenerAuthGuard], component: ScreenedBloodViewComponent},
  { path: 'blood-bank/view/:id', canActivate: [AuthGuard], component: BloodBankViewComponent},
  { path: 'blood-bank/:id', canActivate: [AuthGuard], component: BloodbankComponent},
  { path: 'screening-centre/:id', canActivate: [ScreenerAuthGuard], component: ScreeningcentreComponent},
  { path: 'login', component: LoginComponent},
  { path: 'summary/:id', component: SummaryComponent},
  { path: 'summary', redirectTo: '', pathMatch: 'full'},
  {path: '', component: BcSearchComponent},
  {path: '**', component: Error404Component}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
