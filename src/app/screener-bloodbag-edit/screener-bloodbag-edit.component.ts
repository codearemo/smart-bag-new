import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { CodeSearchService } from '../code-search.service';

@Component({
  selector: 'app-screener-bloodbag-edit',
  templateUrl: './screener-bloodbag-edit.component.html',
  styleUrls: ['./screener-bloodbag-edit.component.css']
})
export class ScreenerBloodbagEditComponent implements OnInit {

  confirmResults = false;
  isLoading;
  bbCode;

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: ''
  };

  constructor(private activatedRoute: ActivatedRoute, private codeSearchService: CodeSearchService, private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    this.bbCode = this.activatedRoute.snapshot.paramMap.get('id');
    this.quickBagCheck();
  }

  // Check if the blood bank was assigned to the blood bag
  // Just to be sure they don't use the url to check just any bag
  // Don't worry you don't have to say it...I know i'm smart ;)
  quickBagCheck() {
    this.codeSearchService.getBagDetails(this.bbCode)
      .subscribe(data => {
        const authScreener = JSON.parse(localStorage.getItem('userScreener'));
        this.bbDetails = JSON.parse(JSON.stringify(data));
        this.bbDetails.screener = this.bbDetails.screener.split('#')[1];
        if (this.bbDetails.screener !== authScreener.screenerId) {
          alert(`Blood bag ${this.bbCode} doesn't belong to your Screening Centre`);
          this.router.navigate(['/login']);
        }
        this.isLoading = false;
      }, error => {
        alert('Bag may be unavailable or please check network and try again');
        this.router.navigate(['/login']);
        this.isLoading = false;
      });
  }

  screenedResultSubmit() {
    this.confirmResults = true;
  }

  myCheck() {
    // tslint:disable-next-line:max-line-length
    if (this.bbDetails.HCV === 'Positive' || this.bbDetails.HepatitisB === 'Positive' || this.bbDetails.Hepatitisc === 'Positive' || this.bbDetails.HIV === 'Positive' || this.bbDetails.Spyhills === 'Positive') {
      this.bbDetails.safety = 'Unsafe';
      return;
    }
    this.bbDetails.safety = 'Safe';
  }

  sendResult() {
    this.bbDetails.screener = 'resource:org.lifebank.smartbag.Screeningcenter#' + this.bbDetails.screener;
    this.router.navigate([`/screened-blood/${this.activatedRoute.snapshot.paramMap.get('id')}`]);
  }
}
