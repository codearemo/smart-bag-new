import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenerBloodbagEditComponent } from './screener-bloodbag-edit.component';

describe('ScreenerBloodbagEditComponent', () => {
  let component: ScreenerBloodbagEditComponent;
  let fixture: ComponentFixture<ScreenerBloodbagEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenerBloodbagEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenerBloodbagEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
