import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreeningcentreComponent } from './screeningcentre.component';

describe('ScreeningcentreComponent', () => {
  let component: ScreeningcentreComponent;
  let fixture: ComponentFixture<ScreeningcentreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreeningcentreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreeningcentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
