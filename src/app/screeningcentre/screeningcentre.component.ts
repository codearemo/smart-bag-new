import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { CodeSearchService } from '../code-search.service';
import { PostbagsDetailsService } from '../services/postbags-details.service';

@Component({
  selector: 'app-screeningcentre',
  templateUrl: './screeningcentre.component.html',
  styleUrls: ['./screeningcentre.component.css']
})
export class ScreeningcentreComponent implements OnInit {


  testResult = {
    bloodGroup: '',
    HIV: '',
    hepatitisb: '',
    hepatitisc: '',
    syphillis: '',
    HCV: '',
    result: '',
    screeningCode: '',
    PCVtext: '',
    screenerName: ''
  };

  bbDetails = {
    BagId: '',
    PVC: '',
    safety: '',
    Btype: '',
    HIV: '',
    HepatitisB: '',
    Hepatitisc: '',
    Spyhills: '',
    HCV: '',
    Notes: '',
    owner: '',
    screener: ''
  };

  confirmResults = false;
  isLoading;
  bbCode;

  // tslint:disable-next-line:max-line-length
  constructor(private activatedRoute: ActivatedRoute, private codeSearchService: CodeSearchService, private router: Router, private pbd: PostbagsDetailsService) { }

  ngOnInit() {
    this.isLoading = true;
    this.bbCode = this.activatedRoute.snapshot.paramMap.get('id');
    this.quickBagCheck();
    // tslint:disable-next-line:max-line-length
    if (this.bbDetails.HCV === 'Positive' || this.bbDetails.HepatitisB === 'Positive' || this.bbDetails.Hepatitisc === 'Positive' || this.bbDetails.HIV === 'Positive' || this.bbDetails.Spyhills === 'Positive') {
      this.bbDetails.safety = 'Unsafe';
      return;
    }
    this.bbDetails.safety = 'Safe';
  }

  // Check if the blood bank was assigned to the blood bag
  // Just to be sure they don't use the url to check just any bag
  // Don't worry you don't have to say it...I know i'm smart ;)
  quickBagCheck() {
    this.codeSearchService.getBagDetails(this.bbCode)
      .subscribe(data => {
        const authScreener = JSON.parse(localStorage.getItem('userScreener'));
        this.bbDetails = JSON.parse(JSON.stringify(data));
        this.bbDetails.screener = this.bbDetails.screener.split('#')[1];
        if (this.bbDetails.screener === authScreener.screenerId) {
          this.isLoading = false;
        } else {
          alert(`Bag ${this.bbCode} is not assigned to your screening centre`);
          this.router.navigate(['/login']);
          this.isLoading = false;
        }
      }, error => {
        alert('Bag may be unavailable or please check network and try again');
        this.router.navigate(['/login']);
        this.isLoading = false;
      });
  }

  screenedResultSubmit() {
    this.confirmResults = true;
  }

  selectBloodType(event) {
    this.isLoading = true;
    this.pbd.updateBloodType(event.target.value, 'org.lifebank.smartbag.Bag#' + this.bbDetails.BagId)
      .subscribe(data => {
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      });
  }

  changeResult(event) {
    this.isLoading = true;
    const val = 'org.lifebank.smartbag.Bag#' + this.bbDetails.BagId;
    this.pbd.updateBagTestResult(val, event.target.name, event.target.value)
    .subscribe(data => {
      this.checkResult();
    }, error => {
      console.log(error);
      this.isLoading = false;
    });
  }

  checkResult() {
    // tslint:disable-next-line:max-line-length
    if (this.bbDetails.HCV === 'Positive' || this.bbDetails.HepatitisB === 'Positive' || this.bbDetails.Hepatitisc === 'Positive' || this.bbDetails.HIV === 'Positive' || this.bbDetails.Spyhills === 'Positive') {
      this.bbDetails.safety = 'Unsafe';
      this.pbd.updateBagTestResult('org.lifebank.smartbag.Bag#' + this.bbDetails.BagId, 'Safety', this.bbDetails.safety)
        .subscribe(data => {
          this.isLoading = false;
        }, error => {
          console.log(error);
          this.isLoading = false;
        });
      return;
    }
    this.bbDetails.safety = 'Safe';
    this.pbd.updateBagTestResult('org.lifebank.smartbag.Bag#' + this.bbDetails.BagId, 'Safety', this.bbDetails.safety)
      .subscribe(data => {
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      });
  }

  // For the safety change check
  checkSafety() {
    alert('Safety');
  }



  sendResult() {
    this.pbd.updatePCV(this.bbDetails.PVC, 'org.lifebank.smartbag.Bag#' + this.bbDetails.BagId)
      .subscribe(data => {
        this.router.navigate([`/screened-blood/${this.activatedRoute.snapshot.paramMap.get('id')}`]);
      }, error => console.log(error));
  }

}
