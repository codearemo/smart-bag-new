export interface BloodBankModel {
  bankId: string;
  firstName: string;
  lastName: string;
}
