export interface BloodBagModel {
  BagId: string;
  PVC: string;
  safety: string;
  Btype: string;
  HIV: string;
  HepatitisB: string;
  Hepatitisc: string;
  Spyhills: string;
  HCV: string;
  Notes: string;
  owner: string;
  screener: string;
}
