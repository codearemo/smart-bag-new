export interface TestResultModel {
  bloodGroup: string;
  HIV: string;
  hepatitisb: string;
  hepatitisc: string;
  syphillis: string;
  HCV: string;
  result: string;
  screeningCode: string;
  PCVtext: string;
  screenerName: string;
}
